<?php
/**
 * 只有数字字母下划线验证，也就是是否符合变量名
 * @param string $content 要过滤的字符串
 * @return string 返回是否符合，符合为true
 */
function validateVar($content) {
    return preg_match("/^[A-Za-z_][A-Za-z0-9_]*$/", $content);
}

/**
 * 字符串划分处理
 * @param string $delimiter 划分和连接使用的字符串
 * @param array|string $dealArray 需要处理的数组或者字符串
 * @param string|callable $dealFunction 处理函数，需要返回一个字符串
 * @return string 处理完的字符串
 */
function explodeDeal ($delimiter, $dealArray, $dealFunction) {
    assert(is_callable($dealFunction)/*,'$dealFunction 参数需要可执行的'*/);
    if(is_string($dealArray)){
        $dealArray=explode($delimiter,$dealArray);
    }
    foreach($dealArray as &$v){
        $v=$dealFunction($v);
    }
    return implode($delimiter,$dealArray);
}
//TODO 获取用$delimiter划分后的第几项
//function getPieceAt($delimiter,$string,$offset){
//
//}

function json($s){
    return json_decode($s,true);
}