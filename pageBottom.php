<div class="footer">
    <div class="ref">
        <div class="df">
            <div class="foot">
                <h3>精品网站</h3>
                <div class="share-list">
                    <ul>
                        <li><a href="http://ds.hitwh.edu.cn/">哈工大威海校区数据结构</a></li>
                        <li><a href="http://cs.xidian.edu.cn/jpkc/sjjg2/index.htm">西安电子科技大学数据结构</a></li>
                        <li><a href="http://jpkc.fudan.edu.cn/s/256/">复旦大学数据结构</a></li>
                        <li><a href="http://jpkc.nwu.edu.cn/datastr/">西北工业大学数据结构</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="ref">
        <div class="foot">
            <h3>相关课程</h3>
            <div class="share-list">
                <ul>
                    <li><a href="http://www.openke.net/">公开课中心</a></li>
                    <li><a href="http://www.openke.net/show.php?id=516">武汉理工数据结构公开课</a></li>
                    <li><a href="http://www.mooc.cn/news/185.html">北京大学数据结构公开课</a></li>
                    <li><a href="http://www.mooc.cn/news/185.html">清华MOOC数据结构课程</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="ref">
        <div class="foot">
            <h3>兴趣网站</h3>
            <div class="share-list">
                <ul>
                    <li><a href="http://emuch.net/">小木虫</a></li>
                    <li><a href="http://www.ituring.com.cn/">图灵社区</a></li>
                    <li><a href="http://www.moe.edu.cn/">教育部</a></li>
                    <li><a href="http://www.ec.js.edu.cn/">江苏省教育厅</a></li>
                </ul>
            </div>
        </div>
    </div>
        <div class='copyright'>
            Copyright © 2014-2015 All Rights Reserved - 中国矿业大学计算机科学与技术学院
        </div>
</div>