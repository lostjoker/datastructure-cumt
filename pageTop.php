<div class="data">
    <div class="content">
        <div class="logo">
            <h1 ><a href="index.php">数据结构课程</a></h1>
            <h2 ><a href="index.php">Data Structure</a></h2>
        </div>
        <div class="search">
            <form action="search.php" method="GET">
                <input  class="int" type="text" name="content" placeholder="数据结构..."/>
                <input class="inp" type="submit"  value=""/>
            </form>
        </div>
    </div>

    <div class="top">
        <ul class="menu">
            <li class="menu-item"><a href="index.php">网站首页</a></li>
            <li class="menu-item"><a href="intro.php?subtype=课程简介&maintype=课程简介">课程简介</a>   
                <ul class="submenu">
                    <li class="submenu-item"><a href="intro.php?subtype=课程简介&maintype=课程简介">课程简介</a></li>
                    <li class="submenu-item"><a href="intro.php?subtype=课程组&maintype=课程简介">课程组</a></li>
                    <li class="submenu-item"><a href="intro.php?subtype=联系方式&maintype=课程简介">联系方式</a></li>
                </ul>
            </li> 
            <li class="menu-item"><a href="list.php?subtype=通知公告&maintype=通知公告">通知公告</a></li>
            <li class="menu-item"><a href="intro.php?subtype=实验大纲&maintype=课程实验">课程实验</a>
                <ul class="submenu">
                    <li class="submenu-item"><a href="intro.php?subtype=实验大纲&maintype=课程实验">实验大纲</a></li>
                    <li class="submenu-item"><a href="list.php?subtype=实验指导&maintype=课程实验">实验指导</a></li>
                    <li class="submenu-item"><a href="list.php?subtype=实践课题&maintype=课程实验">实践课题</a></li>
                </ul>
            </li>
            <li class="menu-item"><a href="intro.php?subtype=教学纲要&maintype=课程学习">课程学习</a>
                <ul class="submenu">
                    <li class="submenu-item"><a href="intro.php?subtype=教学纲要&maintype=课程学习">教学纲要</a></li>
                    <li class="submenu-item"><a href="list.php?subtype=在线课件&maintype=课程学习">在线课件</a></li>
                </ul>
            </li>
            <li class="menu-item"><a href="download.php?subtype=下载专区&maintype=下载专区">下载专区</a></li>
        </ul>
    </div>
</div>

