<?php 
	require_once("likyhphpLib.php");
	$db = SqlDB::init();
	$subtype=$db->quote($_GET['subtype']);
	$maintype=$db->quote($_GET['maintype']);
	//var_dump($subtype);
	//var_dump($maintype);
	$idm=$db->getValue("select id from `main_type` where maintype=$maintype");
	$ids=$db->getValue("select id  from `sub_type` where subtype=$subtype");
	//var_dump($idm);
	//var_dump($ids);
	$result=$db->getOne("select * from `passage`  where subtype='$ids' and maintype='$idm' limit 1");
	$id=isset($_GET['id'])&& !empty($_GET['id'])? (int)$_GET['id']: $result['id'];
	//var_dump($id);
 	$idPrev=$db->getValue("select max(id) from  `passage`  where subtype='$ids' and maintype='$idm'and id<'$id'");
	$idAfter=$db->getValue("select min(id) from `passage`  where subtype='$ids' and maintype='$idm' and id>'$id'");
	
	function nl2p($text) {
 		return "<p>" . str_replace("\n", "</p><p>", $text) . "</p>";
	}
?><!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="index.css">
	<link rel="stylesheet" type="text/css" href="intro.css">
	<script src="js/jquery.js"></script>
    <script src="js/height.js"></script>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php echo $_GET['subtype']; ?>--数据结构</title>
</head>
<body>
<div id="containers">
<header>
    <?php include('pageTop.php');?>
</header>
<div class="lists">
	<div class="left">
	<div class="leftsub">
		<div class="site">
			<!--<span class="title"><?php  echo $_GET['maintype']; ?></span>-->
			<span class="seat"><a href="pageHome.php">首页</a>>><?php  echo $_GET['maintype']; ?>>><?php echo $_GET['subtype']; ?></span>
		</div>
		<div class="article-title">
    		<h1><?php echo($result['title']);?></h1>
		</div>
		<div class="article-info">
    		<span>【发布日期：<?php echo substr($result['create_time'],0,10);?>】</span>
		</div>
		<div class="artitle-view">
    		<!--<p><?php echo nl2br($result['content']); ?></p> -->
    		<?php echo nl2p($result['content']); ?>
		</div>
		<div class="article-page">  
    		<div class="before">
    		<?php if(empty($idPrev)){ ?>
        		<span class='read_before'>已是第一篇</span>
    		<?php }else{ ?>
        		<a href="intro.php?subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>&id=<?php echo $idPrev;?>"><span class="read_before">上一篇</span></a>
    		<?php } ?>
    		</div>
    		<div class="after">
    		<?php if(empty($idAfter)){ ?>
        		<span class='read_after'>已是最后一篇</span>
    		<?php }else{ ?>
        		<a href="intro.php?subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>&id=<?php echo $idAfter;?>"><span class="read_after">下一篇</span></a>
        	<?php } ?>
    		</div>
		</div>
	</div>
	</div>
	<?php include('right.php') ?>
</div>
<footer>
    <?php include('pageBottom.php') ?>
</footer>
</div>
</body>
<!--<script type="text/javascript">
    /*var height=parseInt($('div.right p').css('height'),10);
    var idm= '<?php echo $idm ?>';
    if(idm==3){
        $('.right ul').css("height",1.7*idm*height);  //课程简介
    }else if(idm==4){
         $('.right ul').css("height",0.9*idm*height);    //课程学习
     }else if(idm==2){
        $('.right ul').css("height",2.7*idm*height);   //课程实验
     }else if(idm==1){
     	$('.right ul').css("height",2.7*idm*height);   
     }*/
     var height=$('.left').css("height");
     $('.right').css('height',height);
     //var height=
</script>-->
</html>