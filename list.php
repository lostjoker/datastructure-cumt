<?php
    require_once("likyhphpLib.php");
    $db=SqlDB::init();
    $page=isset($_GET['page'])&& !empty($_GET['page'])? (int)$_GET['page']: 1;
    $page_size = 10; 
    $page_start = ($page-1)*$page_size ;
    $subtype=$db->quote($_GET['subtype']);
    $maintype=$db->quote($_GET['maintype']);
    $idm=$db->getValue("select id from `main_type` where maintype=$maintype");
    $ids=$db->getValue("select id  from `sub_type` where subtype=$subtype");
    $amount_title=$db->getValue("select count(title)from `passage` where subtype='$ids' and maintype='$idm' ");
    $result=$db->getAll("select * from `passage` where subtype='$ids' and maintype='$idm' order by create_time DESC limit $page_start,$page_size");
    //计算页数 
    if( $amount_title ){
        if( $amount_title < $page_size ){ $page_count = 1; }               
        if( $amount_title % $page_size ){                                   
            $page_count = (int)($amount_title / $page_size) + 1;          
        }else{
            $page_count = $amount_title / $page_size;                     
        }
    }else{
        $page_count = 1;
    }
?><!DOCTYPE html>
<html>
<head>
  
    <link rel="stylesheet" type="text/css" href="intro.css">
    <link rel="stylesheet" type="text/css" href="list.css">
    <link rel="stylesheet" type="text/css" href="index.css">
    <script src="js/jquery.js"></script>
    <script src="js/height.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $_GET['subtype'] ?>--数据结构</title>
</head>
<body>
<div id="containers"> 
<header>
    <?php include('pageTop.php'); ?>
</header>
<div class="lists">
    <div class="left">
    <div class="leftsub">
        <div class="site">
            <!--<span><?php echo $_GET['subtype'] ?></span>-->
            <span class="seat"><a href="index.php">首页</a>>><?php  echo $_GET['maintype']; ?>>><?php echo $_GET['subtype'] ?></span>
        </div>
        <div class="article_list">
            <ul>
            <?php foreach($result as $value){?>
                <li>
                    <img src="<?php echo $value['photo'];?>">
                    <div class="article">
                        <div class="article_title">
                            <a href="intro.php?subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>&id=<?php echo  $value['id'];?>"><?php echo $value['title'];?></a>
                        </div>
                        <div class="article_time">
                            <?php echo substr($value['create_time'],0,10);?>    
                        </div>
                        <div class="sum">
                            <p><?php echo  substr($value['content'],0,220);?><a>......</a></p>
                        </div>
                        <a class="readmore" href="intro.php?subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>&id=<?php echo  $value['id'];?>">更多</a>
                    </div>
                </li>
            <?php }?>
            </ul>
        </div>
   <!--显示分页-->
        <div class="article_page">
            <ul>
                <a href="list.php?page=1&subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>"><span class="page_first">首页</span></a>
        
                <?php if($page!=1){ ?>
                    <a href="list.php?page=<?php echo $page-1?>&subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>"><span class="page_before">上一页</span></a>
                <?php }else{ ?>
                    <span class='page_before'>上一页</span>
                <?php } ?>
        
                <?php for($i=1;$i<=$page_count;$i++){//页面数字显示 ?>
                    <a href="list.php?page=<?php echo $i;?>&subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>">
                        <span class="page_num"><?php  echo "[$i]";?></span>
                    </a>
                <?php } ?>

                <?php if($page!=$page_count){ ?>
                    <a href="list.php?page=<?php echo $page+1;?>&subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>">
                <span class="page_next">下一页</span></a>
        
                <?php }else{echo "<span class='page_next'>下一页</span>";} ?>
                    <a href="list.php?page=<?php echo $page_count?>&subtype=<?php echo $_GET['subtype']; ?>&maintype=<?php echo $_GET['maintype']; ?>">
                <span class="page_last">尾页</span></a>
            </ul>
        </div>
    </div>
    </div>
    <?php include('right.php') ?>
</div>
<footer>
    <?php include('pageBottom.php') ?>
</footer>
</div>
</body>
<!--<script type="text/javascript">
    /*var height=parseInt($('div.right p').css('height'),10);
    var idm= '<?php echo $idm ?>';
    if(idm==3){
        $('.right ul').css("height",1.7*idm*height);
    }else if(idm==4){
        $('.right ul').css("height",0.9*idm*height);
    }else if(idm==2){
        $('.right ul').css("height",2.7*idm*height);
    }else if(idm==1){
        $('.right ul').css("height",1.9*idm*height);   
    }else if(idm==5){
        $('.right ul').css("height",0.4*idm*height);   
    }*/
     var height=$('.left').css("height");
     $('.right').css('height',height);
</script>-->

</html>
