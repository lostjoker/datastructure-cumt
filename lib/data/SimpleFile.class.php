<?PHP
/**
* 新的文件读取写入类
* @author linyh，sg
*/
import("PathGeneration");
class SimpleFile {
    /**
     * 读取一个文件
     * @param string $filename 文件名
     * @return string
     */
    public static function read($filename){
        return file_get_contents($filename);
    }

    /**
     * 对一个文件写入内容，返回写入的字符数，失败则返回-1
     * @param string $path 文件目录
     * @param string $filename 文件名
     * @param string $context 要写入的内容
     * @param null $length 自定义一个允许写入的最大长度
     * @return int|null
     */
    public static function write($path, $filename, &$context, $length=null){
        $path=PathGeneration::getFolder($path);
        if(is_writable($path)){
            $fp = fopen($path.$filename,"wb");
            if(!empty($length)){
                $result=fwrite($fp, $context, $length);
            }else{
                $result=fwrite($fp, $context);
            }
            if($result!==false){
                return $result;
            }
        }
        return -1;
    }

    public static function append($path, $filename, &$context, $length=null){
        $path=PathGeneration::getFolder($path);
        if(is_writable($path.$filename)){
            $fp = fopen($path.$filename,"ab");
            if(!empty($length)){
                $result=fwrite($fp, $context, $length);
            }else{
                $result=fwrite($fp, $context);
            }
            if($result!==false){
                return $result;
            }
        }
        return -1;
    }

}