<?PHP
/**
* 数据库类
* @author linyh,sg
*/
class SqlDB {
    private static $db;
	private static $selfObject;
	
	/**
	* 构造函数，获取pdo引用
	*/
 	private function __construct() {
		if(isset(self::$db)) return;
		
		global $config;
		$host = $config['mysql']["host"];
		$port = $config['mysql']["port"];
		$user = $config['mysql']["username"];
		$pass = $config['mysql']["password"];
		$dbname = $config['mysql']["dbname"];

		try {
			$DSN = "mysql:host={$host};port={$port};dbname={$dbname}";
			$handle = new PDO($DSN,$user,$pass);
			$handle -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			$handle-> query("set names utf8");
            self::$db = $handle;
		} catch(PDOException $e) {
			echo "Connection failed:".$e->getMessage();
		}
		return;
	}
	
	/**
	 * 获取数据库操作类的引用对象
	 */
	static public function init(){
		if(!isset(self::$selfObject)){
			self::$selfObject= new SqlDB();
		}
		return self::$selfObject;
	}

    /**
     * 对id格式化为sql查询语句
     * @param array|int|string $ids
     * @return string
     */
    public function getIdCondition($ids, $idName="id"){
        assert(!empty($ids)/*,"id参数不能为空"*/);
        if(is_array($ids)){
            sort($ids);
            $idCondition="`{$idName}` in (".explodeDeal(",",$ids,function($s){return (int)$s;}). ')';
        }else if(is_string($ids)){
            $idCondition="`{$idName}` in (".explodeDeal(",",$ids,function($s){return (int)$s;}). ')';
        }else if(is_int($ids)){
            $idCondition="`{$idName}`=".(int)$ids;
        }
        return $idCondition;
    }

    /**
     * 对一个字符串加引号并过滤
     * @param string $s
     * @return string
     */
    function quote($s){
        return self::$db->quote($s);
    }
	
	/**
	* 函数直接执行sql语句
	* @param string $sql 要执行的SQL语句
	* @return int 返回修改的行数
	*/
	public function sqlExec($sql){
		$result=self::$db->exec($sql);
		return $result;
	}

	/**
	* 检查sql语句是否能查询到内容
	* @param string $sql 要执行的SQL语句
	* @return bool 是否查询到内容
	*/
	function getExist($sql){
		$tmp=self::$db->query($sql);
		$ifGet = $tmp->fetch(PDO::FETCH_BOUND);
		return $ifGet;
	}

	/**
	* 使用sql语句获取数据
	* 这一句sql必须保证只查询到一行一列
	* @param string $sql 要执行的SQL语句
	* @return string 获取到的内容
	*/
	function getValue($sql){
		$tmp=self::$db->query($sql);
		if(!empty($tmp)){
			$array= $tmp->fetch(PDO::FETCH_NUM);
			return $array[0];
		}else{
			return null;
		}
	}

	/**
	* 函数使用sql语句获取一行数据
	* @param string $sql 要执行的SQL语句
	* @return array 一个1维数组
	*/
	function getOne($sql){
		$tmp=self::$db->query($sql);
		if(!empty($tmp)){
			$array= $tmp->fetch(PDO::FETCH_ASSOC);
			return $array;
		}else{
			return null;
		}
	}

    /**
     * 函数使用sql语句获取所有查询到的数据
     * @param string $sql 要执行的SQL语句
     * @return array 一个2维数组
     */
    function getAll($sql){
        $tmp=self::$db->query($sql);
        if(!empty($tmp)){
            $array= $tmp->fetchall(PDO::FETCH_ASSOC);
            return $array;
        }else{
            return array();
        }
    }

    /**
     * 函数使用sql语句获取所有查询到的数据
     * @param string $sql 要执行的SQL语句
     * @return \SqlDBStatement|null
     */
    function query($sql){
        //TODO 这里要处理错误信息，重要！！需要统一处理
        try {
            $stmt = self::$db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
            $stmt->execute();
            return new SqlDBStatement($stmt);
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
        return null;
    }

    /**
     * 生成复杂的查询语句
     * 如该字段:array('student S'=>array('id','name','num'=>'studentNum'),'project P'=>array('id'=>'project_id','name'));
     * S.id,S.name,S.num as studentNum,P.id as project_id,P.name
     *
     * 如该条件:array('type'=>1,'age'=>'> 5','age '=>'<=12  ','icon'=>'like %.jpg', 'text'=>'', 'S.area'=>null, array('or','S.name','=','sg'),'`class` is not null');
     * type=1 and age>'5' and age<='12' and icon like '%.jpg' and S.name='sg' or `class` is not null
     *
     * @param array $data 要查询的数据
     * @param array $condition 查询使用的条件
     * @param string $attach 查询附加信息
     * @param int $rows 返回行数
     * @param int $offset 从第几行开始返回
     * @param bool $blankRetain 是否开启空白查询模式，默认关闭
     * @return string 返回Sql语句
     */
    public function selectSql($data, $condition, $attach='', $rows=null, $offset=0, $blankRetain=false){
        assert(is_array($data)/*,'$data参数必须是数组'*/);
        assert(!empty($data)/*,'$data参数不能为空'*/);
        assert(is_array($condition)/*,'$condition参数必须是数组'*/);
        assert(!empty($condition)/*,'$condition参数不能为空'*/);
        $fieldArray=array();
        $table=array();

        // 处理查询字符串
        foreach($data as $tableName=>$v){
            if(!is_array($v)){
                // 如果是直接一个参数
                $fieldArray[]=trim($v);
            }else{
                // 如果是表名=>字段名数组
                $tableNameArray=explode(" ",$tableName);
                foreach($tableNameArray as &$tableV) $tableV='`'.trim($tableV).'`';
                $table[]=implode(" ",$tableNameArray);
                $tableName=end($tableNameArray);
                foreach($v as $source=>$target){
                    if(is_string($source)){
                        $fieldArray[]="$tableName.`$source` as `$target`";
                    }else{
                        $fieldArray[]="$tableName.`$target`";
                    }
                }
            }
        }

        $conditionStr='1=1 ';
        foreach($condition as $field=>$judge){
            if(!is_string($field)){
                if(is_array($judge)) {
                    // 形如 array('or','S.name','=','sg')
                    list($link, $field, $op, $value)=$judge;
                }else{
                    // 形如 '`class` is not null'
                    $conditionStr.="and $judge ";
                    continue;
                }
            }else{
                $link="and";
                $judge=trim($judge);
                // 形如 age=>'<=12  '
                if(substr($judge, 0,1)==">"|| substr($judge, 0,1)=="<"|| substr($judge, 0,1)=="="){
                    if(substr($judge, 1,1)=="="){
                        $op=substr($judge, 0,2);
                        $value=trim(substr($judge, 2));
                    }else{
                        $op=substr($judge, 0,1);
                        $value=trim(substr($judge, 1));
                    }
                }elseif(strtolower(substr($judge, 0,5))=="like "){
                    $op=substr($judge, 0,5);
                    $value=trim(substr($judge, 5));
                }else{
                    $op='=';
                    $value=$judge;
                }
            }
            // 给字段名加上反引号
            $field=explodeDeal(".",$field,function($s){
                return '`'.trim($s).'`';
            });

            // 根据值的类型决定生成的sql类型
            $value=trim($value);
            if(empty($value)){
                if($blankRetain) {
                    $value=$this->quote($value);
                }else{
                    continue;
                }
            }elseif(is_numeric($value)){
                $value=(int)$value;
            }else{
                $value=$this->quote($value);
            }

            $conditionStr.="$link $field $op $value ";
        }

        $fieldStr=implode(",",$fieldArray);
        $tableStr=implode(",",$table);
        if(is_null($rows)){
            $sql="select $fieldStr from $tableStr where $conditionStr $attach";
        }else{
            $sql="select $fieldStr from $tableStr where $conditionStr $attach limit $offset,$rows";
        }
        return $sql;
    }

    /**
     * 函数使用sql语句获取所有查询到的数据
     */
    public function select($data, $condition, $attach='', $rows=null, $offset=0, $blankRetain=false){
        return self::$db->query($this->selectSql($data, $condition, $attach, $rows, $offset, $blankRetain));
    }

	/**
     * 插入数据
     * @param string $tableName 要插入的表名
     * @param array $data 要插入的数组，键名为数据库字段名，值为要插入的值
     * @param bool $blankRetain 是否开启空白保留模式
     * @return int 插入了多少内容
     */
	function insert($tableName, $data, $blankRetain=false){
		$db=self::$db;

		if(!is_array($data) || empty($data)){
			return false; //没有要插入的数据
		}else{
            $line_arr=array();	//插入的列
            $value_arr=array();	//插入的值
			foreach($data as $key=>$result){
                $result=trim($result);
                if(!empty($result)|| $blankRetain){
                    $line_arr[]="`{$key}`";
                    $value_arr[]=$db->quote($result);
                }
			}
			$line=implode(",",$line_arr);
			$value=implode(",",$value_arr);
			$sql="INSERT INTO `{$tableName}`({$line})values({$value});";
			return $this->sqlExec($sql);
		}
	}
	function insertId(){
		$db=self::$db;
		return $db->lastInsertId();
	}

	/**
	 * 更新数据
	 * @param string $tableName 表名
 	 * @param array|int $id 要修改的id号，如果id不存在则修改失败
	 * @param array $data 要更新的数据，键名为数据库字段名，值为要插入的值
     * @param bool $blankRetain 是否开启空白保留模式
	 * @return int 修改了多少内容
	 */
	function update($tableName,$id,$data, $blankRetain=false){
		$db=self::$db;
		
		//查询要修改的id号，如果id不存在则修改失败
		$up_arr=array();	//更新的语句
		if(!is_array($data)|| count($data)<=0|| empty($id)){
			return 0; //没有要插入的数据
		}else{
			foreach($data as $key=>$value){
                if(!empty($value)|| $blankRetain){
                    $up_arr[]="`{$key}`=".$db->quote($value);
                }
			}
			$up_str=implode(",",$up_arr);
			$sql="UPDATE {$tableName} set {$up_str} where ".$this->getIdCondition($id);
			return $this->sqlExec($sql);
		}
	}

	/**
	 * 添加或者更新数据，根据id存在与否新建或者修改一条数据
	 * @param string $tableName 表名
	 * @param array|int $id 要修改的id号，如果id不存在则新建一行数据
	 * @param array $data 要更新的数据，键名为数据库字段名，值为要插入的值
     * @param bool $blankRetain 是否开启空白保留模式
	 * @return int 修改了多少内容
	 */
	function modify($tableName,$id,$data, $blankRetain=false){
		// 先修改，若没有修改成功则插入一行
		$updateModifyNum=$this->update($tableName,$id,$data, $blankRetain);
		// var_dump($updateModifyNum);
		if($updateModifyNum){
			return $updateModifyNum;
		}else{
			return $this->insert($tableName,$data, $blankRetain);
		}
	}

	/**
     * 删除数据
     * @param string $tableName 表名
     * @param array|int $id
     * @return int 删除了多少内容
     */
	function delete($tableName,$id){
        if(!empty($id)){
            $sql="DELETE from `{$tableName}` where ".$this->getIdCondition($id);
            return $this->sqlExec($sql);
        }else{
            return 0;
        }
	}
}


/**
 * 数据库结果集，用于foreach循环
 * @author linyh,sg
 */
class SqlDBStatement implements Iterator {
    /**
     * 记录当前是第几条记录
     * @var int
     */
    protected $currentNum=0;
    /**
     * @var array
     */
    protected $currentData;
    // 要处理的PDOStatement
    protected $statement;
    /**
     * @var callable
     */
    protected $dealFunction=null;

    /**
     * @param PDOStatement $s
     */
    public function __construct(PDOStatement $s) {
        $this->statement=$s;
    }

    public function getArray(){
        return iterator_to_array($this,false);
    }

    /**
     * 设置每一行数据的处理方式
     * 参数需要时一个函数，有一个参数，类型为数组，用于处理每一条获取到的数据
     * 注意，最后的时候必须要返回处理完的值
     * @param callable $f
     */
    public function setDealMethod($f){
        if(is_callable($f)){
            $this->dealFunction=$f;
        }
    }

    // Iterator接口函数调用顺序：rewind\next valid current key ……

    public function rewind(){
        $this->currentData=$this->statement->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_FIRST);
        $this->currentNum=0;
    }
    public function next(){
        $this->currentNum++;
        $this->currentData=$this->statement->fetch(PDO::FETCH_ASSOC,PDO::FETCH_ORI_NEXT);
    }
    public function valid(){
        return isset($this->currentData)&&!empty($this->currentData);
    }
    public function current(){
        if($f=$this->dealFunction){
            return $f($this->currentData);
        }else{
            return $this->currentData;
        }
    }
    public function key(){
        return $this->currentNum;
    }
}