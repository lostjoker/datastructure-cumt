<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/12/16
 * Time: 16:07
 */
class Page {
    protected $currentPage;
    protected $total;
    protected $pageSize;
    // 存有数据的Iterator
    protected $data;
    // 页码数组
    protected $pageArray;
    // 总页数
    protected $totalPage;
    // 显示页码周围的页数
    protected $pageOffset=4;
    // 页码生成函数
    protected $pageCallback;
    /**
     * 构造函数，传入参数以便使用
     * @param int $currentPage 当前页码
     * @param int $total 总数
     * @param int $pageSize 每页的大小
     * @param Iterator $data 待显示的数据
     */
    function __construct($currentPage=1, $total, $pageSize=30, $data=null, $pageCallback=null) {
        $this->currentPage = (int)$currentPage;
        $this->total = (int)$total;
        $this->pageSize = (int)$pageSize;
        $this->data = $data;
        $this->pageCallback=$pageCallback;
        $this->cal();
    }

    // 置显示页码周围的页数
    public function setPageOffset($pageOffset) {
        $this->pageOffset = $pageOffset;
    }

    public function setPageCallback($pageCallback) {
        $this->pageCallback = $pageCallback;
    }

    function cal(){
        // 计算总页数
        $this->totalPage=$totalPage=ceil($this->total/$this->pageSize);

        // 校正当前页数
        $currentPage = $this->currentPage;
        $currentPage = $currentPage<=$totalPage? $currentPage:$totalPage;
        $this->currentPage = $currentPage = $currentPage>0? $currentPage:1;

        // 获取页码数组
        $start= $currentPage- $this->pageOffset;
        $start= $start>0? $start:1;
        $end= $start+ $this->pageOffset+ $this->pageOffset;
        $end= $end<=$totalPage? $end:$totalPage;
        $this->pageArray=range($start,$end);
    }

    function getPageArray(){
        return $this->pageArray;
    }
    function getData(){
        return $this->data;
    }
    function getTotal(){
        return $this->total;
    }
    public function getCurrentPage() {
        return $this->currentPage;
    }

    function ifPrev(){
        return $this->currentPage>1;
    }
    function getPrev(){
        return $this->currentPage-1;
    }

    function ifNext(){
        return $this->currentPage< $this->totalPage;
    }
    function getNext(){
        return $this->currentPage+1;
    }
    function getFirst(){
        return 1;
    }
    function getLast(){
        return $this->totalPage;
    }

    function getPageUrl($page){
        if(!empty($this->pageCallback)&& is_callable($this->pageCallback)){
            return call_user_func($this->pageCallback, $page);
        }
        return null;
    }
    function getPageHtml(){
        $s="<div id='dataPage'><ul>
        <li>共<span>{$this->getLast()}</span>页/
            <span>{$this->getTotal()}</span>条记录
            当前是第<span>{$this->getCurrentPage()}</span>页</li>
        <li><a href='{$this->getPageUrl($this->getFirst())}'>首页</a></li> ";
        if($this->ifPrev()) {
            $s.="<li><a href='{$this->getPageUrl($this->getPrev())}'>上一页</a></li> ";
        }
        foreach($this->getPageArray() as $i){
            $s.="<li class='page'><a href='{$this->getPageUrl($i)}'>{$i}</a></li> ";
        }
        if($this->ifNext()) {
            $s.="<li><a href='{$this->getPageUrl($this->getNext())}'>下一页</a></li> ";
        }
        $s.="<li><a href='{$this->getPageUrl($this->getLast())}'>末页</a></li></ul></div>";
        return $s;
    }
}
