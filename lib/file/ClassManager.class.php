<?php
/**
 * 读取custom里面所有类
 *exportCustom()函数
 **/
class ClassManager{

	//获得指定目录里面所有类并生成指定格式
	static function analysisClass($analysisArray){
        $classArray=array();
        foreach($analysisArray as $v){
            self::analysisPath($classArray, $v);
        }
        return $classArray;
	}

    protected static function analysisPath(&$classArray, $dir){
        $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
        foreach($objects as $object){
            $name=$object->getBasename(".class.php");
            if (validateVar($name)) {
                $classArray[$name][]=strtr($object->getPath(),"/\\","..").'.'.$name;
            }
        }
    }
}

