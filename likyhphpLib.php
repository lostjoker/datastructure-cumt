<?php
// 包含系统函数
require_once 'system/lang.system.php';
require_once 'system/base.system.php';
require_once 'system/string.system.php';
require_once 'system/password.system.php';

// 包含系统接口
require_once 'system/coreInterface/RunnableInterface.php';
require_once 'system/coreInterface/RouterInterface.php';
require_once 'system/coreInterface/WebRouterInterface.php';
require_once 'system/coreInterface/HtmlCacheInterface.php';

$config=analysisIniFile("config/run.config.ini");
?>
