 <?php 
    require_once("likyhphpLib.php");
    function show($subtype,$maintype) {
        $db = SqlDB::init();
        $idm=$db->getValue("select id  from `main_type` where maintype='$maintype'");
        $ids=$db->getValue("select id  from `sub_type` where subtype='$subtype'");
        $result=$db->getAll("select * from `passage` where subtype='$ids' and maintype=$idm order by create_time DESC limit 4");
        return $result;
     }
?><!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="download.css">
    <meta charset="utf-8">
    <title>数据结构课程建设</title>
    <script src="js/jquery.js"></script>
    <script src="js/slide.js"></script> 
</head>
<body>
    <div id="containers">
    <header>
        <?php include('pageTop.php');?>
    </header>
    <div class="page">
        <div class="inpage"> 
             <div id="show" rel="autoPlay">
      <div class="img">
          <span>
              <img src="images/1.jpg" />
              <img src="images/2.jpg" />
              <img src="images/3.jpg" />
          </span>
      </div>
     </div>
        </div>
    </div>
    <div class="container">
        <div class="fu">
            <!--通知公告-->
            <div class="odd">
                <div class="gq">
                    <div class="title">
                        <span>通知公告</span>
                        <a class="more" href="list.php?subtype=通知公告&maintype=通知公告">More...</a>
                    </div>
                    <?php $result=show('通知公告','通知公告'); ?>
                    <div class="list">
                        <ul>
                            <?php foreach ($result as $key => $value){ ?>
                                <li>
                                    <img src="<?php echo $value['photo'];?>">
                                    <span class="title">
                                        <a href="intro.php?subtype=通知公告&maintype=通知公告&id=<?php echo $value['id']; ?>"><?php echo $value['title'];?></a>
                                    </span>
                                    <div class="time">
                                        <?php echo substr($value['create_time'],0,10);?> 
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--课程实验-->
            <div class="topa">
                <div class="gq">
                    <div class="title">
                        <span>课程实验</span>
                        <a class="more" href="intro.php?subtype=实验大纲&maintype=课程实验">More...</a>
                    </div>
                    <?php $result=show('实验大纲','课程实验'); ?>
                    <div class="list">
                        <ul>
                        <?php foreach ($result as $key => $value){ ?>
                            <li>
                                <img src="<?php echo $value['photo'];?>">
                                <span class="title">
                                    <a href="intro.php?subtype=实验大纲&maintype=课程实验&id=<?php echo $value['id']; ?>"><?php echo $value['title'];?></a><!--文章内容里有说明和下载地址-->
                                </span>
                                <div class="time">
                                    <?php echo substr($value['create_time'],0,10);?> 
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--课程学习-->
            <div class="odd">
                <div class="gq">
                    <div class="title">
                        <span>课程学习</span>
                        <a class="more" href="list.php?subtype=在线课件&maintype=课程学习">More...</a>
                    </div>
                    <?php $result=show('在线课件','课程学习'); ?>
                    <div class="list">
                        <ul>
                        <?php foreach ($result as $key => $value){ ?>
                            <li>
                                <img src="<?php echo $value['photo'];?>">
                                <span class="title">
                                    <a href="intro.php?subtype=在线课件&maintype=课程学习&id=<?php echo $value['id']; ?>"><?php echo $value['title'];?></a><!--文章内容里有说明和下载地址-->
                                </span>
                                <div class="time">
                                    <?php echo substr($value['create_time'],0,10);?> 
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--下载专区-->
            <div class="topas">
                <div class="gqs">
                    <div class="title">
                        <span>下载专区</span>
                        <a class="more" href="download.php?subtype=下载专区&maintype=下载专区">More...</a>
                    </div>
                    <?php $result=show('下载专区','下载专区'); ?>
                    <div class="lists">
                        <div class ="article_list">
                        <ul>
                        <?php foreach ($result as $key => $value){ ?>
                            <li>
                                <div class="article">
                                    <div class="article_title">
                                        <a href="down.php?file=<?php echo $value['title']; ?>"><?php echo $value['title'];?></a>
                                    </div>
                                <div class="article_time">
                                    <?php echo substr($value['create_time'],0,10);?>    
                                </div>
                                </div>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
    <?php include('pageBottom.php') ?>
    </footer>
    </div>
</body>
</html>